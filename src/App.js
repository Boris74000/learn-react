import React, {useState} from "react";
import Expenses from "./components/Expenses/Expenses";
import NewExpense from "./components/NewExpense/NewExpense";

function App(props) {

    const dummy_expenses = [
        {
            id:'e1',
            date: new Date(2022, 2, 28),
            title: 'Car intrance',
            amount: 297.64
        },
        {
            id:'e2',
            date: new Date(2021, 0, 15),
            title: 'Phone',
            amount: 58
        },
        {
            id:'e3',
            date: new Date(2020, 1, 2),
            title: 'Bar',
            amount: 24.59
        },
        {
            id:'e4',
            date: new Date(2020, 4, 10),
            title: 'Theater',
            amount: 12
        }
    ]

    const [expenses, setExpenses] = useState(dummy_expenses);

    function addExpenseHandler(expense) {
        setExpenses(prevExpenses => {
            return [expense, ...prevExpenses];
        });
    };

    return (
        <div>
            <NewExpense onAddExpense = {addExpenseHandler}/>
            <Expenses items = {expenses}/>
        </div>
    );
}

export default App;
